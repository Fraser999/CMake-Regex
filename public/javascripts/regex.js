var socket = io.connect('http://localhost:3000');

var app = angular.module('regexTester', ['ngAnimate']);
app.controller('regexController', function($scope) {
  $scope.initialise = function() {
    $scope.showDocs = false;
    if (typeof Storage !== void(0)) {
      $scope.regex = typeof localStorage.regex === 'undefined' ? '' : localStorage.regex;
      $scope.testString = typeof localStorage.testString === 'undefined' ? '' : localStorage.testString;
      $scope.replacementString = typeof localStorage.replacementString === 'undefined' ? '' : localStorage.replacementString;
      $scope.subcommand = typeof localStorage.subcommand === 'undefined' ? 'match' : localStorage.subcommand;
      socket.emit('Regex', {regex: $scope.regex,
                            testString: $scope.testString,
                            replacementString: $scope.replacementString,
                            subcommand: $scope.subcommand
                           });
    } else {
      $scope.testString = '';
      $scope.subcommand = 'match';
    }
    $scope.docsShowOrHide = 'Show';
    $scope.showCMakeList = true;
    $scope.cmakeShowOrHide = 'hide';
    $scope.inputsClass = 'inputs_expanded';
    $scope.hasCMakeVersion = false;
    $scope.hasMatchResult = false;
    $scope.hasMatchAllResult = false;
    $scope.hasReplaceResult = false;
    $scope.hasMatchCount = false;
    $scope.hasMatch0 = false;
    $scope.hasMatch1 = false;
    $scope.hasMatch2 = false;
    $scope.hasMatch3 = false;
    $scope.hasMatch4 = false;
    $scope.hasMatch5 = false;
    $scope.hasMatch6 = false;
    $scope.hasMatch7 = false;
    $scope.hasMatch8 = false;
    $scope.hasMatch9 = false;
    $scope.hasError = false;
  };

  $scope.initialise();

  $scope.saveState = typeof Storage !== void(0) ? function() {
    localStorage.regex = $scope.regex;
    localStorage.testString = $scope.testString;
    localStorage.replacementString = $scope.replacementString;
    localStorage.subcommand = $scope.subcommand.toString();
  } : function() {};

  $scope.sendRegex = function() {
    $scope.setLineNumbers();
    socket.emit('Regex', {regex: $scope.regex,
                          testString: $scope.testString,
                          replacementString: $scope.replacementString,
                          subcommand: $scope.subcommand
                         });
    $scope.saveState();
  };

  $scope.hasResult = function(variable) {
    return variable !== undefined && variable !== '';
  };

  socket.on('Matches', function(data) {
    $scope.matches = data;
    $scope.hasCMakeVersion = $scope.hasResult(data.cmakeVersion);
    $scope.hasMatchResult = $scope.hasResult(data.matchResult);
    $scope.hasMatchAllResult = $scope.hasResult(data.matchAllResult);
    $scope.hasReplaceResult = $scope.hasResult(data.replaceResult);
    $scope.hasMatchCount = $scope.hasResult(data.cmakeMatchCount);
    $scope.hasMatch0 = $scope.hasResult(data.cmakeMatch0);
    $scope.hasMatch1 = $scope.hasResult(data.cmakeMatch1);
    $scope.hasMatch2 = $scope.hasResult(data.cmakeMatch2);
    $scope.hasMatch3 = $scope.hasResult(data.cmakeMatch3);
    $scope.hasMatch4 = $scope.hasResult(data.cmakeMatch4);
    $scope.hasMatch5 = $scope.hasResult(data.cmakeMatch5);
    $scope.hasMatch6 = $scope.hasResult(data.cmakeMatch6);
    $scope.hasMatch7 = $scope.hasResult(data.cmakeMatch7);
    $scope.hasMatch8 = $scope.hasResult(data.cmakeMatch8);
    $scope.hasMatch9 = $scope.hasResult(data.cmakeMatch9);
    $scope.hasError = $scope.hasResult(data.error);
    $scope.$apply();
  });

  socket.on('disconnect', function() {
    location.reload(true);
  });

  $scope.setLineNumbers = function() {
    var count = $scope.testString.split(/\r|\r\n|\n/).length + 3;
    if ($scope.subcommand === 'replace') {
      ++count;
    }
    var CMakeLineNumbers = document.getElementById('cmake_line_numbers');
    CMakeLineNumbers.textContent = '';
    for (var i = 1; i <= count; ++i) {
      CMakeLineNumbers.textContent += i + '\n';
    }
  };

  $scope.setLineNumbers();

  $scope.toggleSubcommand = function() {
    $scope.sendRegex();
  };

  $scope.toggleDocs = function() {
    if ($scope.showDocs) {
      $scope.showDocs = false;
      $scope.inputsClass = 'inputs_expanded';
      $scope.docsShowOrHide = 'Show';
    } else {
      $scope.inputsClass = 'inputs_contracted';
      $scope.showDocs = true;
      $scope.docsShowOrHide = 'Hide';
    }
  };

  $scope.toggleCMakeList = function() {
    if ($scope.showCMakeList) {
      $scope.showCMakeList = false;
      $scope.cmakeShowOrHide = 'show';
    } else {
      $scope.showCMakeList = true;
      $scope.cmakeShowOrHide = 'hide';
    }
  };
});
