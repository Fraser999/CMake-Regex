var assert = require('assert');
//var should = require('should');
var http = require('http');
var server = require('../bin/www');

it('should return a 200 response', function(done) {
  var app = server;  // jshint ignore:line
  http.get('http://localhost:3000', function(res) {
    assert.equal(res.statusCode, 200);
    done();
  });
});

it('should return a 404 response', function(done) {
  var app = server;  // jshint ignore:line
  http.get('http://localhost:3000/not_there', function(res) {
    assert.equal(res.statusCode, 404);
    done();
  });
});
