var spawn = require('child_process').spawn;
var util = require('util');
var debug = require('debug')('cmake_regex:handle_stream');
var fs = require('fs');
var path = require('path');

function randomFile() {
  var name = '';
  var pool = 'abcdefghijklmnopqrstuvwxyz0123456789';
  for (var i = 0; i < 10; i++) {
    name += pool.charAt(Math.floor(Math.random() * pool.length));
  }
  name += '.cmake';

  return path.join(__dirname, 'cmake', 'temp', name);
}

function filestring(data) {
  var str = 'cmake_policy(SET CMP0053 NEW)\n';
  str += 'set(Regex "%s")\n';
  str += 'set(TestString [=[%s]=])\n';
  str = util.format(str, data.regex, data.testString);
  if (data.subcommand === 'match') {
    str += 'string(REGEX MATCH "${Regex}" MatchResult "${TestString}")\n';
  } else if (data.subcommand === 'matchAll') {
    str += 'string(REGEX MATCHALL "${Regex}" MatchAllResult "${TestString}")\n';
  } else if (data.subcommand === 'replace') {
    str += 'set(Replace [=[%s]=])\n';
    str = util.format(str, data.replacementString);
    str += 'string(REGEX REPLACE "${Regex}" "${Replace}" ReplaceResult "${TestString}")\n';
  } else {
    // TODO respond with error code or close socket
  }
  str += 'include("${CMAKE_CURRENT_LIST_DIR}/../result.cmake")';
  return str;
}

function formatError(errorStr) {
  debug(errorStr);
  // Handle parse error
  var regex = /cmake\/temp\/[a-z\d]{10}\.cmake:(\d+):\s+(Parse error\.  [\S\s]*)\s*CMake Error:/;  // jscs:ignore maximumLineLength
  var match = regex.exec(errorStr);
  if (match !== null) {
    var line = match[1] - 1;
    var result = 'CMake Error: Error in cmake code at CMakeLists.txt:' + line + ':\n' + match[2];
    return result;
  }

  regex = /(CMake Error at )(cmake\/temp\/[a-z\d]{10}\.cmake):(\d+)((.|\s)*)/;  // jscs:ignore maximumLineLength
  match = regex.exec(errorStr);
  line = match[3];

  result = match[1] + 'CMakeLists.txt:' + line + match[4];
  return result;
}

exports.handleStream = function(socket) {
  socket.on('Regex', function(data) {
    debug(data);
    if (typeof data.regex === 'undefined' || data.regex === '') {
      return;
    }
    if (typeof data.testString === 'undefined') {
      data.testString = '';
    }
    if (typeof data.replacementString === 'undefined') {
      data.replacementString = '';
    }

    var filename = randomFile();
    fs.writeFile(filename, filestring(data), function(err) {
      if (err) {
        // TODO respond with error code or close socket
        return debug(err);
      }

      var responseStr = '';
      var errorStr = '';
      debug('Running cmake -P ' + filename);
      var cmake = spawn('cmake', ['-P', filename]);

      cmake.stdout.on('data', function(data) {
        debug('stdout: ' + data);
        responseStr = String(data).substring(3);
      });

      cmake.stderr.on('data', function(data) {
        debug('stderr: ' + data);
        errorStr += data;
      });

      cmake.on('close', function(code) {
        debug('child process exited with code ' + code);
        var response = {};
        if (code === 0) {
          response = JSON.parse(responseStr);
        } else {
          response.error = formatError(errorStr);
        }
        debug(response);
        socket.emit('Matches', response);
        fs.unlink(filename, function() {});
      });
    });
  });
};
