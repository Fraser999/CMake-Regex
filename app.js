var express = require('express');
var app = express();

var favicon = require('serve-favicon');
var path = require('path');
app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));

var logger = require('morgan');
app.use(logger('dev'));

var serveStatic = require('serve-static');
app.use(serveStatic(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res) {
  if (err.status === 404) {
    res.sendFile('404.html', {'root': path.join(__dirname, 'public')});
  } else {
    res.sendFile('500.html', {'root': path.join(__dirname, 'public')});
  }
});

module.exports = app;
