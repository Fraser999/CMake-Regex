# CMake Regex Tester

### [Live site](http://localhost:3000)

### Still To Do

##### Short term

- ~~Move client-side script in index.html to separate file~~
- ~~Handle CMake warnings and error messages~~
- ~~Fix doc animation for IE~~
- ~~Fix width of tooltips~~
- Fix inconsistent display of match containing CRLF between FF and IE
- ~~Sort favicon~~
- ~~Switch between CMake versions~~
- ~~Switch or have different pages for `MATCH`, `MATCHALL` and `REPLACE`~~
- ~~Finish 404 page~~
- Handle server-side error (close socket/send error?)
- ~~Handle socket closing on index.html~~
- Possibly switch between `"..."` and `[=[...]=]` syntax
- Change all references to `localhost:3000`
- ~~Add links to regex command and add docs description for syntax in hide/show block~~
- Add tests
- Add to travis?
- ~~Use HTML5 local storage to cache regex and to allow "undo"?~~
- Clean up code
- Look at compressing all files
- ~~Change from JQuery to Angular~~
- Add site to monitor.us

##### Long term

- Add page of vars/functions available in different versions of CMake.  Should be able to dynamically choose var/function from list? as well as table?
- Look at using templates for DRYing headers/footers/error pages
- Add button to copy CMakeLists.txt
- Try putting socket on different port
- Switch to show results as raw CMake strings
